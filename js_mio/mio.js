$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('el modal contacto se está mostrando');

        $('#contactoBtn').removeClass("btn btn-warning");
        $('#contactoBtn').addClass("btn btn-secondary");
        $('#contactoBtn').prop("disabled", true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
        console.log('el modal contacto se mostró');
    });

    $('#contacto').on('hide.bs.modal', function (e){
        console.log('el modal contacto se está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
        $('#contactoBtn').prop("disabled", false);
        $('#contactoBtn').removeClass("btn btn-secondary");
        $('#contactoBtn').addClass("btn btn-warning");
    });
       
});